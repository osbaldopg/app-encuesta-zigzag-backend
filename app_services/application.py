from flask import Flask, request, jsonify
from flask_mysqldb import MySQL
from flask_cors import CORS
from cursors import BaseDatos

app = Flask(__name__)
CORS(app)

# Configurar MySQL
app.config['SECRET_KEY' ] = 'thisissecret'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'zigzagQ'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'


# Inicializar MySQL
mysql = MySQL(app)
db = BaseDatos()

# Método para registro
@app.route('/agregar_respuestas', methods=['POST'])
def register():
    # Obtenemos los datos del json que manden
    data = request.get_json()
    return jsonify({'message' : db.nuevo_respuestas(data)})

@app.route('/agregar_respuestas_primer_vez', methods=['POST'])
def register_primer_vez():
    # Obtenemos los datos del json que manden
    data = request.get_json()
    return jsonify({'message' : db.nuevo_respuestas_primer_vez(data)})

@app.route('/obtener_respuestas', methods=['GET'])
def obtener_respuestas():
    return jsonify({"Respuestas":db.obtener_respuestas()})

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
