from flask import Flask, request, jsonify
from flask_mysqldb import MySQL


class BaseDatos:

    def __init__(self):
        self.app = Flask(__name__)

        # Configurar MySQL
        self.app.config['SECRET_KEY' ] = 'thisissecret'
        self.app.config['MYSQL_HOST'] = 'localhost'
        self.app.config['MYSQL_USER'] = 'root'
        self.app.config['MYSQL_PASSWORD'] = ''
        self.app.config['MYSQL_DB'] = 'zigzagQ'
        self.app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

        ## Init MYSQL
        self.mysql = MySQL(self.app)

       

    # Crea un nuevo participante en la bd
    def nuevo_respuestas(self, data):
        es_la_primera_vez = data['_02_es_la_primera_vez']
        cuantas_veces_visitado_ultimos_tres_anios = data['_03_cuantas_veces_visitado_ultimos_tres_anios']
        como_se_entero_de_la_existencia_del_zigzag = data['_04_como_se_entero_de_la_existencia_del_zigzag']
        calificacion_salas_interactivas = data['_05_calificacion_salas_interactivas']
        calificacion_talleres_cientificos = data['_06_calificacion_talleres_cientificos']
        calificacion_exposiciones_temporales = data['_07_calificacion_exposiciones_temporales']
        calificacion_zona_de_aventuras = data['_09_calificacion_zona_de_aventuras']
        prefiere_recorridos_libres_guiados = data['_10_prefiere_recorridos_libres_guiados']
        calificacion_general_zigzag = data['_11_calificacion_general_zigzag']
        aspectos_para_mejorar_zigzag = data['_12_aspectos_para_mejorar_zigzag']
        tipo_de_grupo = data['_13_tipo_de_grupo']
        sexo = data['_14_sexo']
        edad = data['_15_edad']
        # Encriptamos la password
        cur = self.mysql.connection.cursor()
        cur.execute("INSERT INTO respuestas (_02_es_la_primera_vez,_03_cuantas_veces_visitado_ultimos_tres_anios,_04_como_se_entero_de_la_existencia_del_zigzag,"
        "_05_calificacion_salas_interactivas,_06_calificacion_talleres_cientificos,_07_calificacion_exposiciones_temporales,_09_calificacion_zona_de_aventuras,"
        "_10_prefiere_recorridos_libres_guiados,_11_calificacion_general_zigzag,_12_aspectos_para_mejorar_zigzag,_13_tipo_de_grupo,_14_sexo,_15_edad)"
        "VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", ([es_la_primera_vez,cuantas_veces_visitado_ultimos_tres_anios,como_se_entero_de_la_existencia_del_zigzag,
        calificacion_salas_interactivas,calificacion_talleres_cientificos,calificacion_exposiciones_temporales,calificacion_zona_de_aventuras,
        prefiere_recorridos_libres_guiados,calificacion_general_zigzag,aspectos_para_mejorar_zigzag,tipo_de_grupo,sexo,edad]))
        ##Commit to DB
        self.mysql.connection.commit()

        ##Cerrar conexion
        cur.close()

        # Enviamos correo
        return 'Registro correcto'

    def nuevo_respuestas_primer_vez(self, data):
        es_la_primera_vez = data['_02_es_la_primera_vez']
        # Encriptamos la password
        cur = self.mysql.connection.cursor()
        cur.execute("INSERT INTO respuestas (_02_es_la_primera_vez)"
        "VALUES(%s)", ([es_la_primera_vez]))
        ##Commit to DB
        self.mysql.connection.commit()

        ##Cerrar conexion
        cur.close()

        # Enviamos correo
        return 'Registro correcto'

    # Obtener todos los participantes de la bd
    def obtener_respuestas(self):
        try:
            cur = self.mysql.connection.cursor()
            result = cur.execute("SELECT * FROM respuestas")
            data = cur.fetchall()
            cur.close()
            return data
        except:
            return 'Ups! Hemos tenido problemas'
