CREATE DATABASE zigzagQ;

USE zigzagQ;

CREATE TABLE respuestas(
  id_q INT PRIMARY KEY AUTO_INCREMENT,
  _01_marca_temporal TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  _02_es_la_primera_vez VARCHAR(10),
  _03_cuantas_veces_visitado_ultimos_tres_anios INT(2),
  _04_como_se_entero_de_la_existencia_del_zigzag VARCHAR(70),
  _05_calificacion_salas_interactivas INT(2),
  _06_calificacion_talleres_cientificos INT(2),
  _07_calificacion_exposiciones_temporales INT(2),
  _08_calificacion_sendero_sol_viento INT(2),
  _09_calificacion_zona_de_aventuras INT(2),
  _10_prefiere_recorridos_libres_guiados VARCHAR(50),
  _11_calificacion_general_zigzag INT(2),
  _12_aspectos_para_mejorar_zigzag VARCHAR(100),
  _13_tipo_de_grupo VARCHAR(30),
  _14_sexo VARCHAR(30),
  _15_edad INT(3)
);
